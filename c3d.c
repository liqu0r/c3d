#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <Windows.h>
#include <conio.h>
#include <math.h>

#define M_PIF           3.14159265f
#define M_SQRT3INVF     0.57735027f
#define SCREEN_WIDTH    1280
#define SCREEN_HEIGHT   720
#define TICK_RATE       300
#define KEYDOWN(vKey)   ((GetAsyncKeyState(vKey) & 0x8000) ? 1 : 0)

// Math part.
typedef struct { float m[4][4]; } matrix_t;
typedef struct { float x, y, z, w; } vector_t;
typedef vector_t point_t;

float math_Interp(float x1, float x2, float t);
void math_VectorAdd(vector_t* z, const vector_t* x, const vector_t* y);
void math_VectorSub(vector_t* z, const vector_t* x, const vector_t* y);
float math_VectorDotProduct(const vector_t* x, const vector_t* y);
float math_VectorLen(const vector_t* v);
void math_VectorCrossProduct(vector_t* z, const vector_t* x, const vector_t* y);
void math_VectorInterp(vector_t* z, const vector_t* x, const vector_t* y, float t);
void math_VectorNormalize(vector_t* v);
void math_MatrixAdd(matrix_t* c, const matrix_t* a, const matrix_t* b);
void math_MatrixSub(matrix_t* c, const matrix_t* a, const matrix_t* b);
void math_MatrixMul(matrix_t* c, const matrix_t* a, const matrix_t* b);
void math_MatrixScale(matrix_t* c, const matrix_t* a, float f);
void math_MatrixApply(vector_t* y, const vector_t* x, const matrix_t* m);
void math_MatrixSetIdentity(matrix_t* m);
void math_MatrixSetZero(matrix_t* m);
void math_MatrixSetTranslate(matrix_t* m, float x, float y, float z);
void math_MatrixSetScale(matrix_t* m, float x, float y, float z);
void math_MatrixSetRotate(matrix_t* m, float x, float y, float z, float theta);
void math_MatrixSetLookAt(matrix_t* m, const vector_t* eye, const vector_t* at, const vector_t* up);
void math_MatrixSetPerspective(matrix_t *m, float fovy, float aspect, float zn, float zf);

// t in [0,1].
float math_Interp(float x1, float x2, float t)
{
    return x1 + (x2 - x1) * t;
}

// z = x + y.
void math_VectorAdd(vector_t* z, const vector_t* x, const vector_t* y)
{
    z->x = x->x + y->x;
    z->y = x->y + y->y;
    z->z = x->z + y->z;
    z->w = 1.0f;
    return;
}

// z = x - y.
void math_VectorSub(vector_t* z, const vector_t* x, const vector_t* y)
{
    z->x = x->x - y->x;
    z->y = x->y - y->y;
    z->z = x->z - y->z;
    z->w = 1.0f;
    return;
}

// return <x,y>.
float math_VectorDotProduct(const vector_t* x, const vector_t* y)
{
    return x->x * y->x + x->y * y->y + x->z * y->z;
}

// return length of the vector.
float math_VectorLen(const vector_t* v)
{
    return sqrtf(v->x * v->x + v->y * v->y + v->z * v->z);
}

// z = x * y.
void math_VectorCrossProduct(vector_t* z, const vector_t* x, const vector_t* y)
{
    z->x = x->y * y->z - x->z * y->y;
    z->y = x->z * y->x - x->x * y->z;
    z->z = x->x * y->y - x->y * y->x;
    z->w = 1.0f;
    return;
}

// Vector interpolation.
void math_VectorInterp(vector_t* z, const vector_t* x, const vector_t* y, float t)
{
    z->x = math_Interp(x->x, y->x, t);
    z->y = math_Interp(x->y, y->y, t);
    z->z = math_Interp(x->z, y->z, t);
    z->w = 1.0f;
    return;
}

// Vector Normalization.
void math_VectorNormalize(vector_t* v)
{
    float len = math_VectorLen(v);
    if (len != 0.0f) {
        float inv = 1.0f / len;
        v->x *= inv;
        v->y *= inv;
        v->z *= inv;
    }
    return;
}

// c = a + b.
void math_MatrixAdd(matrix_t* c, const matrix_t* a, const matrix_t* b)
{
    for (int i = 0; i < 4; ++i) {
        c->m[i][0] = a->m[i][0] + b->m[i][0];
        c->m[i][1] = a->m[i][1] + b->m[i][1];
        c->m[i][2] = a->m[i][2] + b->m[i][2];
        c->m[i][3] = a->m[i][3] + b->m[i][3];
    }
    return;
}

// c = a - b.
void math_MatrixSub(matrix_t* c, const matrix_t* a, const matrix_t* b)
{
    for (int i = 0; i < 4; ++i) {
        c->m[i][0] = a->m[i][0] - b->m[i][0];
        c->m[i][1] = a->m[i][1] - b->m[i][1];
        c->m[i][2] = a->m[i][2] - b->m[i][2];
        c->m[i][3] = a->m[i][3] - b->m[i][3];
    }
    return;
}

// c = a * b;
void math_MatrixMul(matrix_t* c, const matrix_t* a, const matrix_t* b)
{
    for (int i = 0; i < 4; ++i) {
        for (int j = 0; j < 4; ++j) {
            c->m[i][j] = (a->m[i][0] * b->m[0][j]) +
                (a->m[i][1] * b->m[1][j]) +
                (a->m[i][2] * b->m[2][j]) +
                (a->m[i][3] * b->m[3][j]);
        }
    }
    return;
}

// c = f * a.
void math_MatrixScale(matrix_t* c, const matrix_t* a, float f)
{
    for (int i = 0; i < 4; ++i) {
        c->m[i][0] = a->m[i][0] * f;
        c->m[i][1] = a->m[i][1] * f;
        c->m[i][2] = a->m[i][2] * f;
        c->m[i][3] = a->m[i][3] * f;
    }
    return;
}

// y = x * m.
void math_MatrixApply(vector_t* y, const vector_t* x, const matrix_t* m)
{
    float X = x->x, Y = x->y, Z = x->z, W = x->w;
    y->x = X * m->m[0][0] + Y * m->m[1][0] + Z * m->m[2][0] + W * m->m[3][0];
    y->y = X * m->m[0][1] + Y * m->m[1][1] + Z * m->m[2][1] + W * m->m[3][1];
    y->z = X * m->m[0][2] + Y * m->m[1][2] + Z * m->m[2][2] + W * m->m[3][2];
    y->w = X * m->m[0][3] + Y * m->m[1][3] + Z * m->m[2][3] + W * m->m[3][3];
    return;
}

// Set m to identity matrix.
void math_MatrixSetIdentity(matrix_t* m)
{
    memset(m->m, 0, sizeof(float) << 4);
    m->m[0][0] = m->m[1][1] = m->m[2][2] = m->m[3][3] = 1.0f;
    return;
}

// Set m to zero matrix.
void math_MatrixSetZero(matrix_t* m)
{
    memset(m->m, 0, sizeof(float) << 4);
    return;
}

void math_MatrixSetTranslate(matrix_t* m, float x, float y, float z)
{
    math_MatrixSetIdentity(m);
    m->m[3][0] = x;
    m->m[3][1] = y;
    m->m[3][2] = z;
    return;
}

void math_MatrixSetScale(matrix_t* m, float x, float y, float z)
{
    math_MatrixSetIdentity(m);
    m->m[0][0] = x;
    m->m[1][1] = y;
    m->m[2][2] = z;
    return;
}

void math_MatrixSetRotate(matrix_t* m, float x, float y, float z, float theta)
{
    float hsin = sinf(theta * 0.5f);
    float hcos = cosf(theta * 0.5f);
    float w = hcos;
    vector_t vec;
    vec.x = x;
    vec.y = y;
    vec.z = z;
    vec.w = 1.0f;
    math_VectorNormalize(&vec);
    x = vec.x * hsin;
    y = vec.y * hsin;
    z = vec.z * hsin;
    m->m[0][0] = 1 - 2 * y * y - 2 * z * z;
    m->m[1][0] = 2 * x * y - 2 * w * z;
    m->m[2][0] = 2 * x * z + 2 * w * y;
    m->m[0][1] = 2 * x * y + 2 * w * z;
    m->m[1][1] = 1 - 2 * x * x - 2 * z * z;
    m->m[2][1] = 2 * y * z - 2 * w * x;
    m->m[0][2] = 2 * x * z - 2 * w * y;
    m->m[1][2] = 2 * y * z + 2 * w * x;
    m->m[2][2] = 1 - 2 * x * x - 2 * y * y;
    m->m[0][3] = m->m[1][3] = m->m[2][3] = 0.0f;
    m->m[3][0] = m->m[3][1] = m->m[3][2] = 0.0f;
    m->m[3][3] = 1.0f;
    return;
}

// D3DXMatrixLookAtLH.
void math_MatrixSetLookAt(matrix_t* m, const vector_t* eye, const vector_t* at, const vector_t* up)
{
    vector_t xaxis, yaxis, zaxis;
    math_VectorSub(&zaxis, at, eye);
    math_VectorNormalize(&zaxis);
    math_VectorCrossProduct(&xaxis, up, &zaxis);
    math_VectorNormalize(&xaxis);
    math_VectorCrossProduct(&yaxis, &zaxis, &xaxis);
    m->m[0][0] = xaxis.x;
    m->m[1][0] = xaxis.y;
    m->m[2][0] = xaxis.z;
    m->m[3][0] = -math_VectorDotProduct(&xaxis, eye);
    m->m[0][1] = yaxis.x;
    m->m[1][1] = yaxis.y;
    m->m[2][1] = yaxis.z;
    m->m[3][1] = -math_VectorDotProduct(&yaxis, eye);
    m->m[0][2] = zaxis.x;
    m->m[1][2] = zaxis.y;
    m->m[2][2] = zaxis.z;
    m->m[3][2] = -math_VectorDotProduct(&zaxis, eye);
    m->m[0][3] = m->m[1][3] = m->m[2][3] = 0.0f;
    m->m[3][3] = 1.0f;
    return;
}

// D3DXMatrixPerspectiveFovLH.
void math_MatrixSetPerspective(matrix_t *m, float fovy, float aspect, float zn, float zf)
{
    float fax = 1.0f / tanf(fovy * 0.5f);
    math_MatrixSetZero(m);
    m->m[0][0] = fax / aspect;
    m->m[1][1] = fax;
    m->m[2][2] = zf / (zf - zn);
    m->m[3][2] = -zn * zf / (zf - zn);
    m->m[2][3] = 1.0f;
    return;
}
// End of math part.

// Coordinate transformation.
typedef struct {
    matrix_t world;
    matrix_t view;
    matrix_t projection;
    matrix_t transform;
} transform_t;

void transform_Update(transform_t* ts);
void transform_Init(transform_t* ts);
void transform_Apply(vector_t* y, const vector_t* x, const transform_t* ts);
int transform_CheckCVV(const point_t* v);
void transform_Homogenize(vector_t* y, const vector_t* x);

void transform_Update(transform_t* ts)
{
    matrix_t m;
    math_MatrixMul(&m, &ts->world, &ts->view);
    math_MatrixMul(&ts->transform, &m, &ts->projection);
    return;
}

void transform_Init(transform_t* ts)
{
    float aspect = (SCREEN_WIDTH * 1.0f) / SCREEN_HEIGHT;
    math_MatrixSetIdentity(&ts->world);
    math_MatrixSetIdentity(&ts->view);
    // tan(FOVx / 2) = tan(FOVy / 2) * aspect.
    // Set FOVx to 60 deg.
    float fovy = 2 * atanf(M_SQRT3INVF / aspect);
    math_MatrixSetPerspective(&ts->projection, fovy, aspect, 1.0f, 500.0f);
    transform_Update(ts);
    return;
}

// y = x * ts;
void transform_Apply(vector_t* y, const vector_t* x, const transform_t* ts)
{
    math_MatrixApply(y, x, &ts->transform);
    return;
}

// Check whether the point is in the Canonical View Volume.
int transform_CheckCVV(const point_t* v)
{
    float w = v->w;
    if (v->z < 0.0f) { return 0; }
    if (v->z > w) { return 0; }
    if (v->y < -w) { return 0; }
    if (v->y > w) { return 0; }
    if (v->x < -w) { return 0; }
    if (v->x > w) { return 0; }
    return 1;
}

void transform_Homogenize(vector_t* y, const vector_t* x)
{
    float rw = 1.0f / x->w;
    y->x = (x->x * rw + 1.0f) * SCREEN_WIDTH * 0.5f;
    y->y = (1.0f - x->y * rw) * SCREEN_HEIGHT * 0.5f;
    y->z = x->z * rw;
    y->w = 1.0f;
    return;
}
// End of coordinate transformation part.

typedef struct {
    BYTE*			bRGB;
    unsigned int	uiWidth;
    unsigned int	uiHeight;
    size_t			uiSize;
} VIDMEM;
typedef struct { int x, y; } coord_t;
typedef struct { point_t* v; size_t n; } mesh_t;

VIDMEM* NewVidMem(const unsigned int _uiWidth, const unsigned int _uiHeight);
void CleanVidMem(VIDMEM* _ptr);
void SetVidMemPixel(VIDMEM* _ptr, const int _x, const int _y, const DWORD _rgb);
BYTE PeekVidMemPixelR(const VIDMEM* _ptr, const int _x, const int _y);
BYTE PeekVidMemPixelG(const VIDMEM* _ptr, const int _x, const int _y);
BYTE PeekVidMemPixelB(const VIDMEM* _ptr, const int _x, const int _y);
void DrawVidMem(const HDC _hDst, const VIDMEM* _ptr, const int _fps);
void DeleteVidMem(VIDMEM* _ptr);
void InitConsole(const HANDLE _hout, const HWND _hwnd);
int render_CheckCVV(const coord_t* _v);
void render_DrawLine(VIDMEM* _ptr, const coord_t* _v1, const coord_t* _v2, const DWORD _color);
void render_DrawTriangle(VIDMEM* _ptr, const coord_t* _v1, const coord_t* _v2, const coord_t* _v3, const DWORD _color);
void render_PointTransform(coord_t* v, const point_t* pt, const transform_t* ts);
void WriteFrameToVidMem(VIDMEM* _ptr, transform_t* _ts, const mesh_t* _mesh, const vector_t* _axis, float _theta);

int main(void)
{
    HANDLE hOut = GetStdHandle(STD_OUTPUT_HANDLE);
    HWND hCon = GetConsoleWindow();
    HDC hConDC = GetDC(hCon);
    InitConsole(hOut, hCon);
    VIDMEM* vidmem_ptr = NewVidMem(SCREEN_WIDTH, SCREEN_HEIGHT);
    point_t cube[8] = {
        { 1.0f, -1.0f, 1.0f, 1.0f },
        { -1.0f, -1.0f, 1.0f, 1.0f },
        { -1.0f, 1.0f, 1.0f, 1.0f },
        { 1.0f, 1.0f, 1.0f, 1.0f },
        { 1.0f, -1.0f, -1.0f, 1.0f },
        { -1.0f, -1.0f, -1.0f, 1.0f },
        { -1.0f, 1.0f, -1.0f, 1.0f },
        { 1.0f, 1.0f, -1.0f, 1.0f }
    };
    mesh_t mesh;
    mesh.v = cube;
    mesh.n = 8;
    transform_t transform;
    const int TICK_LEN = 1000000 / TICK_RATE;
    int fps = -1;
    transform_Init(&transform);
    point_t eye = { 8.0f, 0.0f, 0.0f, 1.0f }, at = { 0.0f, 0.0f, 0.0f, 1.0f }, up = { 0.0f, 0.0f, 1.0f, 1.0f };
    math_MatrixSetLookAt(&transform.view, &eye, &at, &up);
    transform_Update(&transform);
    const float step = 0.01f;
    if (vidmem_ptr != NULL) {
        LARGE_INTEGER liBegin, liEnd, liTime, liFreq;
        QueryPerformanceFrequency(&liFreq);
        while (1) {
            QueryPerformanceCounter(&liBegin);
            if (_kbhit()) {
                if (KEYDOWN(0x41)) {
                    vector_t axis = { 0.0f, 0.0f, 1.0f, 0.0f };
                    WriteFrameToVidMem(vidmem_ptr, &transform, &mesh, &axis, step);
                }// Press A key.
                if (KEYDOWN(0x44)) {
                    vector_t axis = { 0.0f, 0.0f, -1.0f, 0.0f };
                    WriteFrameToVidMem(vidmem_ptr, &transform, &mesh, &axis, step);
                }// Press D key.
                if (KEYDOWN(0x57)) {
                    vector_t axis = { 0.0f, -1.0f, 0.0f, 0.0f };
                    WriteFrameToVidMem(vidmem_ptr, &transform, &mesh, &axis, step);
                }// Press W key.
                if (KEYDOWN(0x53)) {
                    vector_t axis = { 0.0f, 1.0f, 0.0f, 0.0f };
                    WriteFrameToVidMem(vidmem_ptr, &transform, &mesh, &axis, step);
                }// Press S key.
                if (KEYDOWN(VK_UP)) {
                    vector_t axis = { 0.0f, 0.0f, 0.0f, 0.0f };
                    eye.x += 0.03f;
                    math_MatrixSetLookAt(&transform.view, &eye, &at, &up);
                    transform_Update(&transform);
                    WriteFrameToVidMem(vidmem_ptr, &transform, &mesh, &axis, 0.0f);
                }// Press UP_ARROW key.
                if (KEYDOWN(VK_DOWN)) {
                    vector_t axis = { 0.0f, 0.0f, 0.0f, 0.0f };
                    eye.x -= 0.03f;
                    math_MatrixSetLookAt(&transform.view, &eye, &at, &up);
                    transform_Update(&transform);
                    WriteFrameToVidMem(vidmem_ptr, &transform, &mesh, &axis, 0.0f);
                }// Press DOWN_ARROW key.
                if (KEYDOWN(VK_ESCAPE)) {
                    break;
                }// Press ESC key.
            }
            else {
                vector_t axis = { 0.0f, 0.0f, 0.0f, 0.0f };
                WriteFrameToVidMem(vidmem_ptr, &transform, &mesh, &axis, 0.0f);
            }
            while (1) {
                QueryPerformanceCounter(&liEnd);
                liTime.QuadPart = liEnd.QuadPart - liBegin.QuadPart;
                liTime.QuadPart *= 1000000;
                if (liTime.QuadPart > TICK_LEN*liFreq.QuadPart) {
                    fps = (int)((1000000 * liFreq.QuadPart) / liTime.QuadPart);
                    DrawVidMem(hConDC, vidmem_ptr, fps);
                    break;
                }
            }
        }
    }
    DeleteVidMem(vidmem_ptr);
    free(vidmem_ptr);
    vidmem_ptr = NULL;
    ReleaseDC(hCon, hConDC);
    DeleteDC(hConDC);
    DestroyWindow(hCon);
    CloseHandle(hOut);
    return 0;
}

VIDMEM* NewVidMem(const unsigned int _uiWidth, const unsigned int _uiHeight)
{
    VIDMEM* ptr = (VIDMEM*)malloc(sizeof(VIDMEM));
    if (ptr == NULL) {
        return NULL;
    }
    ptr->uiSize = (size_t)(_uiWidth * _uiHeight * 3);
    ptr->bRGB = (BYTE*)malloc(ptr->uiSize);
    if (ptr->bRGB == NULL) {
        return NULL;
    }
    ptr->uiWidth = _uiWidth;
    ptr->uiHeight = _uiHeight;
    return ptr;
}

void CleanVidMem(VIDMEM* _ptr)
{
    memset(_ptr->bRGB, 0, _ptr->uiSize);
    return;
}

void SetVidMemPixel(VIDMEM* _ptr, const int _x, const int _y, const DWORD _rgb)
{
    int _offset = ((_ptr->uiHeight - 1 - _y)*_ptr->uiWidth + _x) * 3;
    BYTE* pRGB = _ptr->bRGB + _offset;
    *pRGB++ = (BYTE)(_rgb >> 16);
    *pRGB++ = (BYTE)(_rgb >> 8);
    *pRGB++ = (BYTE)(_rgb);
    return;
}

BYTE PeekVidMemPixelR(const VIDMEM* _ptr, const int _x, const int _y)
{
    int _offset = ((_ptr->uiHeight - 1 - _y) * _ptr->uiWidth + _x) * 3;
    return _ptr->bRGB[_offset + 2];
}

BYTE PeekVidMemPixelG(const VIDMEM* _ptr, const int _x, const int _y)
{
    int _offset = ((_ptr->uiHeight - 1 - _y) * _ptr->uiWidth + _x) * 3;
    return _ptr->bRGB[_offset + 1];
}

BYTE PeekVidMemPixelB(const VIDMEM* _ptr, const int _x, const int _y)
{
    int _offset = ((_ptr->uiHeight - 1 - _y) * _ptr->uiWidth + _x) * 3;
    return _ptr->bRGB[_offset];
}

void DrawVidMem(const HDC _hDst, const VIDMEM* _ptr, const int _fps)
{
    if (_ptr == NULL) {
        return;
    }
    BITMAPINFO bmpInfo;
    bmpInfo.bmiHeader.biBitCount = 24;
    bmpInfo.bmiHeader.biCompression = BI_RGB;
    bmpInfo.bmiHeader.biPlanes = 1;
    bmpInfo.bmiHeader.biSize = sizeof(bmpInfo.bmiHeader);
    bmpInfo.bmiHeader.biWidth = SCREEN_WIDTH;
    bmpInfo.bmiHeader.biHeight = SCREEN_HEIGHT;
    HDC hCDC = CreateCompatibleDC(_hDst);
    HBITMAP hBMP = CreateDIBSection(hCDC, &bmpInfo, DIB_RGB_COLORS, NULL, NULL, 0);
    SelectObject(hCDC, hBMP);
    SetDIBitsToDevice(hCDC, 0, 0, SCREEN_WIDTH, SCREEN_HEIGHT, 0, 0, 0, SCREEN_HEIGHT, _ptr->bRGB, &bmpInfo, DIB_RGB_COLORS);
    BitBlt(_hDst, 0, 0, SCREEN_WIDTH, SCREEN_HEIGHT, hCDC, 0, 0, SRCCOPY);
    if (_fps >= 0) {
        SetTextColor(_hDst, RGB(0, 100, 0));
        char text_buf[10];
        sprintf(text_buf, "%d fps", _fps);
        TextOut(_hDst, 0, 0, text_buf, (int)strlen(text_buf));
    }
    DeleteObject(hBMP);
    DeleteDC(hCDC);
    return;
}

void DeleteVidMem(VIDMEM* _ptr)
{
    if (_ptr != NULL) {
        free(_ptr->bRGB);
    }
    return;
}

void InitConsole(const HANDLE _hout, const HWND _hwnd)
{
    SetWindowLong(_hwnd, GWL_STYLE, GetWindowLong(_hwnd, GWL_STYLE) & ~WS_MAXIMIZEBOX & ~WS_SIZEBOX);
    CONSOLE_FONT_INFOEX infoex = { 0 };
    infoex.cbSize = sizeof(CONSOLE_FONT_INFOEX);
    infoex.dwFontSize.X = 0;
    infoex.dwFontSize.Y = 1;
    SetCurrentConsoleFontEx(_hout, 0, &infoex);
    CONSOLE_CURSOR_INFO cursor_info;
    cursor_info.bVisible = 0;
    cursor_info.dwSize = 1;
    SetConsoleCursorInfo(_hout, &cursor_info);
    char buf[50];
    sprintf(buf, "mode con cols=%d lines=%d", SCREEN_WIDTH, (SCREEN_HEIGHT + 1) / 2);
    system(buf);
    SetConsoleTitle("c3d");
    return;
}

int render_CheckCVV(const coord_t* _v)
{
    return (_v->x == 1 << 31) ? 0 : 1;
}

void render_DrawLine(VIDMEM* _ptr, const coord_t* _v1, const coord_t* _v2, const DWORD _color)
{
    if (render_CheckCVV(_v1) && render_CheckCVV(_v2)) {
        coord_t _p1 = *_v1;
        int dx = abs(_v2->x - _p1.x), sx = (_p1.x < _v2->x) ? 1 : -1;
        int dy = abs(_v2->y - _p1.y), sy = (_p1.y < _v2->y) ? 1 : -1;
        int err = ((dx > dy) ? dx : -dy) / 2, e2;
        while (1) {
            SetVidMemPixel(_ptr, _p1.x, _p1.y, _color);
            if (_p1.x == _v2->x && _p1.y == _v2->y) { break; }
            e2 = err;
            if (e2 > -dx) { err -= dy; _p1.x += sx; }
            if (e2 < dy) { err += dx; _p1.y += sy; }
        }
    }
    return;
}

void render_DrawTriangle(VIDMEM* _ptr, const coord_t* _v1, const coord_t* _v2, const coord_t* _v3, const DWORD _color)
{
    render_DrawLine(_ptr, _v1, _v2, _color);
    render_DrawLine(_ptr, _v1, _v3, _color);
    render_DrawLine(_ptr, _v2, _v3, _color);
    return;
}

void render_PointTransform(coord_t* v, const point_t* pt, const transform_t* ts)
{
    v->x = 1 << 31;
    v->y = 0;
    point_t c, p;
    transform_Apply(&c, pt, ts);
    if (transform_CheckCVV(&c)) {
        transform_Homogenize(&p, &c);
        v->x = (int)floorf(p.x);
        v->y = (int)floorf(p.y);
    }
    return;
}

void WriteFrameToVidMem(VIDMEM* _ptr, transform_t* _ts, const mesh_t* _mesh, const vector_t* _axis, float _theta)
{
    CleanVidMem(_ptr);
    matrix_t m, n;
    math_MatrixSetRotate(&m, _axis->x, _axis->y, _axis->z, _theta);
    math_MatrixMul(&n, &_ts->world, &m);
    _ts->world = n;
    transform_Update(_ts);
    coord_t* v = (coord_t*)malloc(_mesh->n * sizeof(coord_t));
    for (size_t i = 0; i < _mesh->n; ++i) {
        render_PointTransform(v + i, _mesh->v + i, _ts);
    }
    DWORD color = RGB(0, 255, 255);
    render_DrawLine(_ptr, v, v + 1, color);
    render_DrawLine(_ptr, v + 1, v + 2, color);
    render_DrawLine(_ptr, v + 2, v + 3, color);
    render_DrawLine(_ptr, v + 3, v, color);
    render_DrawLine(_ptr, v, v + 4, color);
    render_DrawLine(_ptr, v + 1, v + 5, color);
    render_DrawLine(_ptr, v + 2, v + 6, color);
    render_DrawLine(_ptr, v + 3, v + 7, color);
    render_DrawLine(_ptr, v + 4, v + 5, color);
    render_DrawLine(_ptr, v + 5, v + 6, color);
    render_DrawLine(_ptr, v + 6, v + 7, color);
    render_DrawLine(_ptr, v + 7, v + 4, color);
    render_DrawLine(_ptr, v + 7, v, color);
    render_DrawLine(_ptr, v + 4, v + 1, color);
    render_DrawLine(_ptr, v + 5, v + 2, color);
    render_DrawLine(_ptr, v + 6, v + 3, color);
    render_DrawLine(_ptr, v + 1, v + 3, color);
    render_DrawLine(_ptr, v + 5, v + 7, color);
    free(v);
    return;
}
